#!/usr/bin/env python3

import requests
import time
import datetime
import zipfile
from multiprocessing import Process
import subprocess
import signal

BASE_URL = "https://gitlab.com/api/v4/projects/43752623/"

with open(".gitlab-token", 'r') as token_file:
    PRIVATE_TOKEN = token_file.readline().strip()

def request_jobs():
    REQUEST_URL = BASE_URL + "jobs?scope[]=success"
    headers = {'PRIVATE-TOKEN': PRIVATE_TOKEN}
    res = requests.get(REQUEST_URL, headers=headers)
    if res.status_code != requests.codes.ok:
        print(f"Failed to request jobs: {res.status_code}")
        return
    return res.json()

def pick_most_recent_job(jobs):
    most_recent_finish = None
    job_id = None
    for job in jobs:
        finished_at = datetime.datetime.strptime(job["finished_at"], "%Y-%m-%dT%H:%M:%S.%fZ")
        if most_recent_finish is None or most_recent_finish < finished_at:
            most_recent_finish = finished_at 
            job_id = job["id"]
    return job_id

def run_app():
    #while True:
        res = subprocess.run(["java", "-jar", "output/target/main-0.0.1-SNAPSHOT.jar"])
        print("App process exited with code", res.returncode)
        time.sleep(1)
        print("Restarting...")

app_proc_handle = None
def restart_process():
    global app_proc_handle
    if not app_proc_handle is None:
        print("Killing the previous process")
        app_proc_handle.kill()
        # to make sure the port is free
        time.sleep(1)
    app_proc_handle = subprocess.Popen(["java", "-jar", "output/target/main-0.0.1-SNAPSHOT.jar"])

def request_job_artifact():
    REQUEST_URL = BASE_URL + f"jobs/artifacts/main/download?job=build"
    headers = {'PRIVATE-TOKEN': PRIVATE_TOKEN}
    res = requests.get(REQUEST_URL, headers=headers)
    if res.status_code != requests.codes.ok:
        print(f"Failed to request jobs: {res.status_code}")
        return
    with open("output.zip", "wb") as output:
        output.write(res.content)
        print("Successfully written new archive")
    with zipfile.ZipFile("output.zip", 'r') as zip_ref:
        zip_ref.extractall("output")
    

if __name__ == '__main__' :
    last_job_id = 0

    def signal_handler(sig, frame):
        if not app_proc_handle is None: app_proc_handle.kill()
        exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    
    while True:
        jobs = request_jobs()
        if jobs is None: continue
        recent_id = pick_most_recent_job(jobs)
        if recent_id != last_job_id:
            print(f"New successful job {recent_id}")
            request_job_artifact()
            restart_process()
            last_job_id = recent_id
        time.sleep(1)